<jsp:include page="/includes/body/includes_before_body.jsp" />
<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
      <li class="breadcrumb-item"><a href="celebrate.jsp">Celebrate</a></li>
      <li class="breadcrumb-item"><a href="pre_customize.jsp">Pre-Customize</a></li>
      <li class="breadcrumb-item active">Checkout</li>
    </ol>
    <div class="col-xs-12">
        <div class="progress">
            <div class="bar">
                <div class="progress-bar progress-bar-striped active" role="progressbar" 
                     aria-valuenow="40" aria-valuemin="0" 
                     aria-valuemax="100" style="width:75%">
                    <span class="sr-only">75% Complete</span>
                    75% Complete
                </div>
           </div><!--end bar-->     
        </div><!--end progress-->
    </div><!--end cols-->
</div>
<div class="container body_contents">
<div class="col-xs-12 table-responsive text-center"><!--table responsive works only with cols in the div-->
    <table class="table table-condensed table-hover">
        <thead>
            <th>#</th>
            <th>Item</th>
            <th>Description</th>
            <th>Type</th>
            <th>Modify</th>
            <th>Price</th>
        </thead>
        <tbody>
            <tr>
                <td class="col-xs-1">1</td>
                <td class="col-xs-4"><a href="#"><img src="images/pre_customize/carousel/cakes/image 1.jpg" class="img-responsive img-circle"></a></td>
                <td class="col-xs-3"><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></td>
                <td class="col-xs-1">Type I</td>
                <td class="col-xs-1">
                    <a href="#" class="btn btn-sm col-xs-12">Edit</a>
                    <a id="danger" href="#" class="btn btn-sm col-xs-12">Delete</a></td>
                <td class="col-xs-1"><span class="badge">69.69</span></td>
            </tr>
            <tr>
                <td class="col-xs-1">2</td>
                <td class="col-xs-4"><a href="#"><img src="images/pre_customize/carousel/cakes/image 2.jpg" class="img-responsive img-circle"></a></td>
                <td class="col-xs-3"><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></td>
                <td class="col-xs-1">Type II</td>
                <td class="col-xs-1">
                    <a href="#" class="btn btn-sm col-xs-12">Edit</a>
                    <a id="danger" href="#" class="btn btn-sm col-xs-12">Delete</a></td>
                <td class="col-xs-1"><span class="badge">69.69</span></td>
            </tr>
            <tr>
                <td class="col-xs-1">3</td>
                <td class="col-xs-4"><a href="#"><img src="images/pre_customize/carousel/cakes/image 3.jpg" class="img-responsive img-circle"></a></td>
                <td class="col-xs-3"><p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p></td>
                <td class="col-xs-1">Type III</td>
                <td class="col-xs-1">
                     <a href="#" class="btn btn-sm col-xs-12">Edit</a>
                     <a id="danger" href="#" class="btn btn-sm col-xs-12">Delete</a></td>
                <td class="col-xs-1"><span class="badge">69.69</span></td>
            </tr>
            <tr>
                <td></td>                
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: right"><b>Total</b></td>
                <td><span class="badge">69.69</span></td>
            </tr>
        </tbody>
    </table>
</div><!--end table-responsive class-->
    
    <div class="col-xs-12 col-sm-4 col-sm-offset-4">
        <a href="payment.jsp" class="btn btn-lg">Purchase</a>
    </div>
</div><!--end container-->
<jsp:include page="/includes/body/includes_after_body.jsp" />