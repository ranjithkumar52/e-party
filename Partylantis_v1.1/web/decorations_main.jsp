<jsp:include page="includes/body/includes_before_body.jsp" />
<div class="container well">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h4 class="text-center">Select any decoration made for you</h4>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 11.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration I <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 12.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration II <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 26.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration III <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 5.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration IV <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 9.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration V <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 6.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration VI <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 11.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration VII <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 26.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration VIII <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 5.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Decoration IX <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
</div><!--end container-->
<jsp:include page="includes/body/includes_after_body.jsp" />