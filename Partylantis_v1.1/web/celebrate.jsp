<jsp:include page="/includes/body/includes_before_body.jsp" />
<div class="container body_contents_inside">
    <ol class="breadcrumb body_contents_inside">
      <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
      <li class="breadcrumb-item active">Celebrate</li>
    </ol> 
    <div class="col-xs-12">
        <div class="progress body_contents_inside">
            <div class="bar">
                <div class="progress-bar progress-bar-striped active" role="progressbar" 
                     aria-valuenow="40" aria-valuemin="0" 
                     aria-valuemax="100" style="width:25%">
                    <span class="sr-only">25% Complete</span>
                    25% Complete
                </div>
           </div><!--end bar-->     
        </div><!--end progress-->
    </div><!--end cols-->
</div>

<!--body-->


<div class="container-fluid body_contents">
    <div class="row">
        <div class="col-xs-12 col-sm-4 col-sm-offset-2 tile text-center">
            <div class="row tile-header">
                <p>"Lorem ipsum dolor sit amet, consectetur</p>
            </div><!--end tile-header-->
             <div class="row tile-content">
                 <a href="customize.jsp"><img src="images/home/buttons/customize.jpg" alt="item 1" class="img-responsive img-thumbnail" data-toggle="tooltip" data-placement="bottom" title="Customize your party"></a>
            </div><!--end tile-content-->
            <div class="row tile-footer">
                <p>"Lorem ipsum dolor sit amet, consectetur</p>
            </div><!--end tile-footer-->
        </div><!--end tile-->
        <div class="col-xs-12 col-sm-4 tile text-center">
            <div class="row tile-header">
                <p>"Lorem ipsum dolor sit amet, consectetur</p>
            </div><!--end tile-header-->
             <div class="row tile-content">
                <a href="pre_customize.jsp"><img src="images/home/buttons/pre-customize.jpg" alt="item 2" class="img-responsive img-thumbnail" data-toggle="tooltip" data-placement="bottom" title="Check our packages made for you"></a>
            </div><!--end tile-content-->
            <div class="row tile-footer">
                <p>"Lorem ipsum dolor sit amet, consectetur</p>
            </div><!--end tile-footer-->
        </div><!--end tile-->
    </div>
</div><!--end container-fluid-->


<jsp:include page="/includes/body/includes_after_body.jsp" />