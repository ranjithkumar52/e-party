<jsp:include page="includes/body/includes_before_body.jsp" />
<div class = "container">
    <div class="row">
        <div>
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active">
                    <a href="#past_orders" aria-controls="package 1" role="tab" data-toggle="tab"><p>Past Orders</p></a>
                </li>
                <li role="presentation" class="">
                    <a href="#current_orders" aria-controls="package 2" role="tab" data-toggle="tab"><p>Current Orders</p></a>
                </li>
            </ul>
        </div>
    </div><!--/row-->
    <div class="row">
        <div class="tab-content">
            <div role="tabpanel" class="row tab-pane fade in active" id="past_orders">
                <div class="well">
                    <h4 class="page-header lead">My order: I</h4>
                    <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a href="#" class="btn btn-primary btn-sm">Write Feedback</a>
                </div>
                <div class="well">
                    <h4 class="page-header lead">My order: II</h4>
                    <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a href="#" class="btn btn-primary btn-sm">Write Feedback</a>
                </div>
                <div class="well">
                    <h4 class="page-header lead">My order: III</h4>
                    <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a href="#" class="btn btn-primary btn-sm">Write Feedback</a>
                </div>
            </div><!--end cakes-->

            <div role="tabpanel" class="row tab-pane" id="current_orders">
                <div class="well">
                    <h4 class="page-header lead">My order: I</h4>
                    <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a href="#" class="btn btn-warning btn-sm">Change Order</a>
                    <a href="#" class="btn btn-danger btn-sm">Cancel Order</a>
                </div>
                <div class="well">
                    <h4 class="page-header lead">My order: II</h4>
                    <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a href="#" class="btn btn-warning btn-sm">Change Order</a>
                    <a href="#" class="btn btn-danger btn-sm">Cancel Order</a>
                </div>
                <div class="well">
                    <h4 class="page-header lead">My order: III</h4>
                    <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    <a href="#" class="btn btn-warning btn-sm">Change Order</a>
                    <a href="#" class="btn btn-danger btn-sm">Cancel Order</a>
                </div>
            </div><!--end flowers-->       

        </div><!--end tab-content-->
    </div><!--/row-->
        
</div><!--end container-->
<jsp:include page="includes/body/includes_after_body.jsp" />