<jsp:include page="/includes/body/includes_before_body.jsp" />  
<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
      <li class="breadcrumb-item"><a href="celebrate.jsp">Celebrate</a></li>
      <li class="breadcrumb-item active">Customize</li>
    </ol>
</div>
<!-- =============================================================================================================================== -->
<!-- ========================================================   CUSTOMIZE ======================================================================= -->
<div class = "container body_contents">
    <div class="row">
        <div class="col-xs-12">
            <h3>Celebrating is just few steps away!</h3>
        </div><!--end cols-->
        
        <div class="col-xs-12">
            <div class="progress">
                <div class="bar">
                    <div class="progress-bar progress-bar-striped active" role="progressbar" 
                         aria-valuenow="40" aria-valuemin="0" 
                         aria-valuemax="100" style="width:50%">
                        <span class="sr-only">50% complete</span>
                        40% Complete Success
                    </div>
               </div><!--end bar-->     
            </div><!--end progress-->
        </div><!--end cols-->
    </div><!--end row-->
    
    <!--accordion-->
    <div class="panel-group body_contents" id="accordion" role="tablist">
        <!--Cakes-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" 
                       href="#collapse1">Cakes</a>
                </h4><!--panel title-->
            </div><!--heading-->
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h4 class="text-center">Select any cakes made for you</h4>
                    </div><!--end cols-->
                    <!--start cakes-->
                    <jsp:include page="/includes/customize/cake.jsp" />
                    <!--END CAKES-->
            </div><!--panel-body-->
        </div><!--end panel collapse-->
    </div><!--panel default-->
        
        <!--Flower Bouquet-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <a data-toggle="collapse" data-parent="#accordion" 
                       href="#collapse2">Flower Bouquet</a>
                </h4><!--panel title-->
            </div><!--heading-->

            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h4 class="text-center">Select any flower Bouquet made for you</h4>
                    </div><!--end cols-->
                    <!-- start flowers -->
                    <jsp:include page="/includes/customize/flower.jsp" />
                    <!-- end flowers -->
                </div><!--end panel body-->
            </div><!--collapse2-->
        </div><!--panel default-->

        <!--Gifts-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <a data-toggle="collapse" data-parent="#accordion" 
                       href="#collapse3">Gifts</a>
                </h4><!--panel title-->
            </div><!--heading-->

            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h4 class="text-center">Select any gifts made for you</h4><br>
                    </div><!--end cols-->
                    <jsp:include page="/includes/customize/gift.jsp" />
                </div><!--end panel body-->
            </div><!--collapse2-->
        </div><!--panel default-->

        <!--Surprise-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <a data-toggle="collapse" data-parent="#accordion" 
                       href="#collapse4">Surprise</a>
                </h4><!--panel title-->
            </div><!--heading-->

            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h4 class="text-center">Select any surprises made for you</h4>
                    </div><!--end cols-->
                    <jsp:include page="/includes/customize/surprise.jsp" />
                </div><!--end panel body-->
            </div><!--collapse2-->
        </div><!--panel default-->
        
        <!--Venue-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <a data-toggle="collapse" data-parent="#accordion" 
                       href="#collapse5">Venue</a>
                </h4><!--panel title-->
            </div><!--heading-->

            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h4 class="text-center">Select any venue made for you</h4>
                    </div><!--end cols-->
                    <jsp:include page="/includes/customize/venue.jsp" />
                </div><!--end panel body-->
            </div><!--collapse2-->
        </div><!--panel default-->
        
        <!--Decorations-->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title text-center">
                    <a data-toggle="collapse" data-parent="#accordion" 
                       href="#collapse6">Decorations</a>
                </h4><!--panel title-->
            </div><!--heading-->

            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h4 class="text-center">Select any decorations made for you</h4>
                    </div><!--end cols-->
                    <jsp:include page="/includes/customize/decoration.jsp" />
                </div><!--end panel body-->
            </div><!--collapse2-->
        </div><!--panel default-->
    </div><!--close panel -->
    
    <div class="row body_contents">
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
            <input type="submit" value="Buy" 
                           class="btn btn-lg">
        </div><!--cols-->
    </div><!--rows-->
    
</div><!--end container-->
<jsp:include page="/includes/body/includes_after_body.jsp" />
