<jsp:include page="/includes/body/includes_before_body.jsp" />  
    <!--body-->
    <div class="container body_contents_inside">
        <div class="row">
            <div class="col-xs-12">
                <ol class="breadcrumb body_contents_inside">
                    <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
                    <li class="breadcrumb-item active">About Us</li>
                  </ol> 
            </div><!--end cols-->
        </div><!--end row-->
        
        <div class="row body_contents">
            <div class="col-xs-12">
                <div class="well col-sm-6">
                    <h1 class="page-header">What is partylantis?</h1>
                    <p class="lead text-justify">Partylantis is a one stop destination for everyone who wants to celebrate 
                        the most important occasions of life right from birthdays to bridal showers filling them 
                        with exuberant surprises, entertainment & excitement.</p>
                </div><!--end cols-->
                <div class="well col-sm-6">
                    <h1 class="page-header">What do we do?</h1>
                    <p class="lead text-justify">
                        The idea is to give a flexibility to customize your own party packages with just a click.<br>
                        The themes we offer you gives you the exact mood of the party you might want to have.</p>
                </div><!--end cols-->
                <div class="well col-sm-6 col-sm-offset-3">
                    <h1 class="page-header">Our Aim</h1>
                    <p class="lead text-justify">The regime of Partylantis is to spread unfettered joy and happiness and make your loved ones feel special and pampered.</p>
                </div><!--end cols-->
            </div><!--end cols-->
        </div><!--end row-->
    </div><!--end container-->
<jsp:include page="/includes/body/includes_after_body.jsp" />