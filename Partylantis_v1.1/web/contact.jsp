<jsp:include page="/includes/body/includes_before_body.jsp" />  
<!--FEEDBACK FORM-->
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                <ol class="breadcrumb body_contents_inside">
                    <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
                    <li class="breadcrumb-item active">About Us</li>
                  </ol> 
            </div><!--end cols-->
        </div><!--end row-->
        <form class="form-horizontal" role="form">
            <div class="form-group row body_contents">
                <div class="col-xs-12 text-center">
                    <p>Please send us your feedback or contact us at <b>celebrate@partylantis.com</b></p>
                </div><!--end of cols-->

                <label for="email" class="sr-only">Email</label>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                    <input type="email" class="form-control" name="email" id="email"
                           placeholder="Email" maxlength="50" required>
                </div><!--end cols-->

                <label for="contact_message" class="sr-only">Message</label>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-6 col-md-offset-3">
                    <textarea class="form-control" rows="10" id="contact_message" 
                              placeholder="Message" ></textarea>
                </div><!--end cols-->

                <div class="col-xs-12 col-sm-4 col-sm-offset-4 col-md-4 col-md-offset-4 body_contents">
                    <input type="submit" id="contact_submit" value="Send Message" 
                           class="btn btn-lg">
                </div><!--end cols-->

            </div><!--end form group row-->
        </form><!--end form-->
    </div><!--end container-->
<jsp:include page="/includes/body/includes_after_body.jsp" />