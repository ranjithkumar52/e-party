<jsp:include page="/includes/body/includes_before_body.jsp" />
<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
      <li class="breadcrumb-item"><a href="celebrate.jsp">Celebrate</a></li>
      <li class="breadcrumb-item"><a href="pre_customize.jsp">Pre-Customize</a></li>
      <li class="breadcrumb-item"><a href="checkout.jsp">Checkout</a></li>
      <li class="breadcrumb-item active">Payment</li>
    </ol>
    <div class="col-xs-12">
        <div class="progress">
            <div class="bar">
                <div class="progress-bar progress-bar-striped active" role="progressbar" 
                     aria-valuenow="40" aria-valuemin="0" 
                     aria-valuemax="100" style="width:100%">
                    <span class="sr-only">Woah! You done it.</span>
                    100% Complete (Woah! You done it.)
                </div>
           </div><!--end bar-->     
        </div><!--end progress-->
    </div><!--end cols-->
</div>
<div class="container body_contents">
    <form>
        <div class="col-xs-12 col-sm-4 col-sm-offset-4 text-center form-group">
            <input type="radio" class="form-check" name="cash_on_delivery" value="cod" checked disabled> Cash on Delivery
        </div><!--end form group-->
    
        <div class="col-xs-12 col-sm-4 col-sm-offset-4">
             <a href="#" class="btn btn-lg" role="button">Confirm payment</a>
        </div>
    </form>
</div><!--end container-->
<jsp:include page="/includes/body/includes_after_body.jsp" />