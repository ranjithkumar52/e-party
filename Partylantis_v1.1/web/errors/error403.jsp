<!DOCTYPE html>
<html lang ="en-US">
    <head> 
        <!--Container for meta data -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- viewport enables to set the width of the website to device width -->
        <!-- The above 3 meta tags *must* come first in the head; any other head
        content must come *after* these tags -->
        <!--Used for search engines -->
        <meta name="keywords" content="party,partylantis,planning,hosting">
        <meta name="description" content="One stop destination to host your party">
        <!--bootstrap-->
        <link href="styles/bootstrap.min.css" type="text/css" rel="stylesheet">
        <link href="styles/bootstrap-theme.min.css" type="text/css" rel="stylesheet">
        <!--social media icons-->
        <link href="styles/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link href="styles/bootstrap-social.css" type="text/css" rel="stylesheet">
        <!--custom css file-->
        <link rel="stylesheet" type="text/css" href="styles/home.css"/>
        <!--image and logo for the taskbar in the browser-->
        <link rel="shortcut icon"  href="${pageContext.request.contextPath}/images/body/mask-balloon.jpg" />
        <!--google fonts-->
        <link href='//fonts.googleapis.com/css?family=Amiri' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Amatic+SC' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Forum' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Imprima' rel='stylesheet'>
        <link href='//fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet'>
        <title>403 - Forbidden</title>
    </head>
    <body>
        <center class="body_contents">
            <h4>Sorry, Cake is <b>(FORBIDDEN)</b>. Please go back to <a href="home.jsp">home</a> page.</h4>
        </center>
    </body>
</html>