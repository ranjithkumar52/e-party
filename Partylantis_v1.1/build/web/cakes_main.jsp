<jsp:include page="includes/body/includes_before_body.jsp" />
<div class="container well">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h4 class="text-center">Select any cakes made for you</h4>
            <br>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="item_description.jsp"><img src="images/misc/Website 11.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Cake I </h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                  <p>Brand: Brand I</p>
                  <p>Type: Type I</p>
                  <p>Flavor: Flavor I</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 12.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake II <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 26.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake III <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 5.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake IV <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 9.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake V <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 6.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake VI <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 11.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake VII <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 26.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake VIII <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="#"><img src="images/misc/Website 5.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Cake IX <span class="badge">69.69</span></h4>
              <p class="">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
              <a href="#" class="btn btn-success btn-block">Add to Cart</a>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
</div><!--end container-->
<jsp:include page="includes/body/includes_after_body.jsp" />