<!-- =============================================================================================================================== -->
<!-- ========================================================   SIGN UP ======================================================================= -->
<div class="container">
    <div id="signupModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <!--CONTENT-->
            <div class="modal-content border-debug">
                
                <!--modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p id="signup_modal_title" class="lead modal-title text-capitalize text-center">Join us to celebrate. The party awaits!</p>
                </div><!--modal header-->
                
                <!--modal body-->
                <div class="modal-body">
                    <jsp:include page="/includes/signup/signup_include.jsp"/>
                </div><!--modal body-->
                
                <!--modal footer-->
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-sm-6">
                                <input type="submit" id="signup_submit" value="Sign Up & Celebrate" class="btn btn-default btn-block" >
                            </div><!--end cols-->
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                            </div><!--end cols-->
                            <!--<div class="text-center" id="terms_and_conditions"><small class="form-text text-muted"><a href="#">By clicking on this you're accepting T&C</a></small></div>-->
                        </div><!--end col-->
                    </div><!--end row-->
                </div><!--modal footer-->
                
            </div><!--modal content-->
        </div><!--base modal dialog -->
    </div><!--modal start-->
</div><!--container-->
