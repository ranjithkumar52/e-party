<jsp:include page="/includes/body/includes_before_body.jsp" />
<div class="container">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="home.jsp">Home</a></li>
      <li class="breadcrumb-item active">Pre-Customize</li>
    </ol>
    <%--
    <div class="col-xs-12">
        <div class="progress">
            <div class="bar">
                <div class="progress-bar progress-bar-striped active" role="progressbar" 
                     aria-valuenow="40" aria-valuemin="0" 
                     aria-valuemax="100" style="width:50%">
                    <span class="sr-only">50% Complete</span>
                    50% Complete
                </div>
           </div><!--end bar-->     
        </div><!--end progress-->
    </div><!--end cols-->--%>
</div><!--end container-->    
<!-- =============================================================================================================================== -->
<!-- ========================================================   PRE-CUSTOMIZE ======================================================================= -->        
<div class = "container">
  
    
    <!-- =============================================================================================================================== -->
    <!-- ========================================================   TAB NAV  ======================================================================= -->
    <div class="">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#package_1" aria-controls="package 1" role="tab" data-toggle="tab"><h3>Package 1</h3></a>
            </li>
            <li role="presentation" class="">
                <a href="#package_2" aria-controls="package 2" role="tab" data-toggle="tab"><h3>Package 2</h3></a>
            </li>
        </ul>
    </div>
    
    <div class="tab-content">
        <div role="tabpanel" class="row tab-pane fade in active" id="package_1">
            <jsp:include page="/includes/pre_customize/package 1.jsp" />
        </div><!--end cakes-->
        
        <div role="tabpanel" class="row tab-pane" id="package_2">
            <jsp:include page="/includes/pre_customize/package 2.jsp" />
        </div><!--end flowers-->       
        
    </div><!--end tab-content-->
       
    <%--
    <div class="row body_contents">
        <div class="col-sm-8 col-sm-offset-2">
            <div class="col-xs-12 col-sm-6">
                <a href="checkout.jsp" class="btn btn-lg">Select Package 1</a>
            </div><!--cols-->
            <div class="col-xs-12 col-sm-6">
                <a href="checkout.jsp" class="btn btn-lg">Select Package 2</a>
            </div><!--cols-->
        </div>
    </div>--%>
</div><!--end container-->
<jsp:include page="/includes/body/includes_after_body.jsp" />            
