<jsp:include page="includes/body/includes_before_body.jsp" />
<div class="container">
    <form class="form well" action="#" method="post" role="form">
        <div class="row text-center">
            <p class="page-header lead">Update your personal details below</p>
        </div><!--/row-->
        <!--============================ FIRST NAME LAST NAME =======================-->
        <div class="form-group row">
            <label for="firstName" class="sr-only">First Name</label>
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-heart"></span>
                    </span>
                    <input type="text" class="form-control" 
                       name="firstname" id="firstName" placeholder="First Name*" 
                       maxlength="50" required >
                </div><!--end input group-->
            </div><!--end cols-->
        </div>
        <div class="form-group row">
            <label for="lastName" class="sr-only">Last Name</label>
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-heart-empty"></span>
                    </span>
                    <input type="text" class="form-control" 
                       name="lastname" id="lastName" placeholder="Last Name*" 
                       maxlength="50" required> 
                </div><!--end input-group-->
                
            </div>
        </div><!--end row-->
        <!--============================ EMAIL PASSWORD =======================-->
        <div class="form-group row">
            <label for="email" class="sr-only">Email</label>
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-envelope"></span>
                    </span>
                    <input type="email" class="form-control" name="email" id="email" placeholder="Email ID*" size="20" maxlength="50" required> 
                </div><!--end input-group-->
            </div><!--end cols-->
        </div><!--end row-->
        <div class="form-group row">
            <label for="password" class="sr-only">Password</label>
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-eye-close"></span>
                    </span>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password*" size="20" maxlength="20" required >
                </div><!--end input-group-->
            </div>
        </div><!--end row-->
        <!--============================ DATE MONTH YEAR =======================-->
        <div class="form-group row">
            <div class="col-sm-offset-2">
                <div class="col-xs-12 col-sm-3">
                    <select class="form-control input-md" name="date" id="date" required>
    <option value="" selected disabled>Date</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
</select>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <select class="form-control input-md" name="month" id="month" required>
    <option value="" selected disabled>Month</option>
    <option value="Jan">Jan</option>
    <option value="Feb">Feb</option>
    <option value="Mar">Mar</option>
    <option value="Apr">Apr</option>
    <option value="May">May</option>
    <option value="Jun">Jun</option>
    <option value="Jul">Jul</option>
    <option value="Aug">Aug</option>
    <option value="Sep">Sep</option>
    <option value="Oct">Oct</option>
    <option value="Nov">Nov</option>
    <option value="Dec">Dec</option>
</select>
                </div>
                <div class="col-xs-12 col-sm-3">
                    <select class="form-control input-md" name="year" id="year" required>
    <option value="2018">2018</option>
    <option value="2017">2017</option>
    <option value="2016">2016</option>
    <option value="2015">2015</option>
    <option value="2014">2014</option>
    <option value="2013">2013</option>
    <option value="2012">2012</option>
    <option value="2011">2011</option>
    <option value="2010">2010</option>
    <option value="2009">2009</option>
    <option value="2008">2008</option>
    <option value="2007">2007</option>
    <option value="2006">2006</option>
    <option value="2005">2005</option>
    <option value="2004">2004</option>
    <option value="2003">2003</option>
    <option value="2002">2002</option>
    <option value="2001">2001</option>
    <option value="2000">2000</option>
    <option value="1999">1999</option>
    <option value="1998">1998</option>
    <option value="1997">1997</option>
    <option value="1996">1996</option>
    <option value="1995">1995</option>
    <option value="1994">1994</option>
    <option value="1993">1993</option>
    <option value="1992">1992</option>
    <option value="1991">1991</option>
    <option value="1990">1990</option>
    <option value="1989">1989</option>
    <option value="1988">1988</option>
    <option value="1987">1987</option>
    <option value="1986">1986</option>
    <option value="1985">1985</option>
    <option value="1984">1984</option>
    <option value="1983">1983</option>
    <option value="1982">1982</option>
    <option value="1981">1981</option>
    <option value="1980">1980</option>
    <option value="1979">1979</option>
    <option value="1978">1978</option>
    <option value="1977">1977</option>
    <option value="1976">1976</option>
    <option value="1975">1975</option>
    <option value="1974">1974</option>
    <option value="1973">1973</option>
    <option value="1972">1972</option>
    <option value="1971">1971</option>
    <option value="1970">1970</option>
    <option value="1969">1969</option>
    <option value="1968">1968</option>
    <option value="1967">1967</option>
    <option value="1966">1966</option>
    <option value="1965">1965</option>
    <option value="1964">1964</option>
    <option value="1963">1963</option>
    <option value="1962">1962</option>
    <option value="1961">1961</option>
    <option value="1960">1960</option>
    <option value="1959">1959</option>
    <option value="1958">1958</option>
    <option value="1957">1957</option>
    <option value="1956">1956</option>
    <option value="1955">1955</option>
    <option value="1954">1954</option>
    <option value="1953">1953</option>
    <option value="1952">1952</option>
    <option value="1951">1951</option>
    <option value="1950">1950</option>
    <option value="1949">1949</option>
    <option value="1948">1948</option>
    <option value="1947">1947</option>
    <option value="1946">1946</option>
    <option value="1945">1945</option>
    <option value="1944">1944</option>
    <option value="1943">1943</option>
    <option value="1942">1942</option>
    <option value="1941">1941</option>
</select>
                </div>
            </div><!--outer cols-->
        </div>
        <!--============================ TELEPHONE NUMBER =======================-->
        <div class="form-group row">
            <label for="telephoneNumber" class="sr-only">Telephone Number</label>
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-phone"></span>
                    </span>
                    <span class="input-group-addon">
                        <span>+91</span>
                    </span>
                    <input type="tel" class="form-control" name="telephonenumber" id="telephoneNumber" placeholder="Phone Number*" maxlength="10" required >
                </div>
                <!--<div class="text-center"><small class="form-text text-muted">Disclaimer: Services available only in Hyderabad</small></div>-->
            </div><!--end col-->
        </div><!--end form group and row-->
        <br>
        <div class="row">
            <div class="col-xs-12">
                <div class="col-sm-6">
                    <input type="submit" id="#" value="Save Changes" class="btn btn-success btn-block" >
                </div><!--end cols-->
                <div class="col-sm-6">
                    <button type="button" class="btn btn-danger btn-block">Cancel</button>
                </div><!--end cols-->
                <!--<div class="text-center" id="terms_and_conditions"><small class="form-text text-muted"><a href="#">By clicking on this you're accepting T&C</a></small></div>-->
            </div><!--end col-->
        </div><!--end row-->
    </form>
</div><!--end container-->
<jsp:include page="includes/body/includes_after_body.jsp" />