<jsp:include page="includes/body/includes_before_body.jsp" />
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="personal.jsp"><img src="" alt="personal" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Personal Details </h4>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="orders.jsp"><img src="" alt="orders" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">My Orders</h4>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="payment.jsp"><img src="" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header  text-center">Payment Options</h4>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
</div><!--/container-->
<jsp:include page="includes/body/includes_after_body.jsp" />