<jsp:include page="/includes/body/includes_before_body.jsp" />
<jsp:include page="/includes/home/carousel_includes.jsp"/>  

<div class="container">
    <%--
    <div class="row body_contents">
        <div class="col-xs-12 col-sm-6">
          <div class="thumbnail">
            <a href="customize.jsp"><img src="images/home/buttons/customize.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Customize your package</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-6">
          <div class="thumbnail">
            <a href="pre_customize.jsp"><img src="images/home/buttons/pre-customize.jpg" alt="item 2" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Pre-customize your package</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
    --%>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3 text-center">
            <h3 class="page-header">Check our items present in the store</h3>
        </div><!--end cols-->
    </div><!--end row-->
    
    <div class="row">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="cakes_main.jsp"><img src="images/misc/Website 11.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Cakes</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="cakes_main.jsp"><img src="images/misc/Website 12.jpg" alt="item 2" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Flowers</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="cakes_main.jsp"><img src="images/misc/Website 9.jpg" alt="item 2" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Gifts</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
    
    
    
    <div class="row body_contents">
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="cakes_main.jsp"><img src="images/misc/Website 6.jpg" alt="item 1" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Surprises</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="cakes_main.jsp"><img src="images/misc/Website 5.jpg" alt="item 2" class="img-responsive img-thumbnail"></a>
            <div class="caption">
              <h4 class="page-header text-center">Venues</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
        <div class="col-xs-12 col-sm-4">
          <div class="thumbnail">
            <a href="cakes_main.jsp"><img src="images/misc/Website 26.jpg" alt="item 2" class="img-responsive img-thumbnail" ></a>
            <div class="caption">
              <h4 class="page-header text-center">Decorations</h4>
              <p class="text-center">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
            </div><!--/caption-->
          </div><!--end thumbnail-->
        </div><!--end cols-->
    </div><!--end row-->
</div><!--end container-->



<%-- template for tile 
<div class="container-fluid body_contents">
    <div class="row">
        <div class="col-xs-12 col-sm-4 tile">
            <div class="row tile-header">
                
            </div><!--end tile-header-->
             <div class="row tile-content">
                
            </div><!--end tile-content-->
            <div class="row tile-footer">
                
            </div><!--end tile-footer-->
        </div><!--end tile-->
    </div>
</div><!--end container-fluid-->
--%>

<jsp:include page="/includes/body/includes_after_body.jsp" />
