<!-- =============================================================================================================================== -->
<!-- ========================================================   LOGIN ======================================================================= -->
<div class="container ">
    <div id="loginModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-md">
            <!--CONTENT-->
            <div class="modal-content">
                
                <!--modal header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <p class="modal-title text-center lead text-capitalize">Enter your details</p>
                </div><!--modal header-->
                
                <!--modal body-->
                <div class="modal-body">
                    <jsp:include page="/includes/login/login_include.jsp"/>
                </div><!--modal body-->
                
                <!--modal footer-->
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="col-sm-6">
                                <input type="submit" name="login" value="Login" class="btn btn-default btn-block" >
                            </div><!--end button cols-->
                            <div class="col-sm-6">
                                <button type="button" class="btn btn-default btn-block" data-dismiss="modal">Cancel</button>
                            </div><!--end button cols-->
                        </div><!--outer col-->
                    </div><!--end row-->
                </div><!--modal footer-->
            </div><!--modal content-->
        </div><!--base modal dialog -->
    </div><!--modal start-->
</div><!--container-->
