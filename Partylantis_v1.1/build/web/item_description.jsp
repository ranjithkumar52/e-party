<jsp:include page="includes/body/includes_before_body.jsp" />
<div class="container">
    <div class="row well">
        <div class="col-sm-4 col-xs-12">
            <img src="images/misc/Website 11.jpg" alt="item 1" class="img-responsive img-thumbnail">
        </div><!--/img cols-->
        <div class="col-sm-8 col-xs-12">
            <div>
                  <h4 class="page-header text-center">Cake I <span class="badge">69.69</span></h4>
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                  <p>Brand: Brand I</p>
                  <p>Type: Type I</p>
                  <p>Flavor: Flavor I</p>
                  <a href="#" class="btn btn-success btn-sm"> Add to Cart</a>
            </div><!--/caption-->
        </div><!--/description cols-->
    </div>
</div><!--/container-->



<jsp:include page="includes/body/includes_after_body.jsp" />