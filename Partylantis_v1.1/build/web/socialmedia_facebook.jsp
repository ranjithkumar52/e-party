<jsp:include page="/includes/body/includes_before_body.jsp" />  
        
<!--body content here-->
<!-- Facebook Live feed -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container body_contents">
    <div class="row">
        <div class="col-md-12 text-center center-block">
            <div class="fb-page"
                 data-href="https://www.facebook.com/Partylantis/" 
                 data-tabs="timeline" 
                 data-width="500" 
                 data-height="500" 
                 data-small-header="false" 
                 data-adapt-container-width="true" 
                 data-hide-cover="false" 
                 data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/Partylantis/" 
                                class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/Partylantis/">
                            Partylantis
                        </a>
                    </blockquote>
            </div><!--end fb-page-->
        </div><!--end cols-->
    </div><!--end row-->
</div><!--end container-->
        
<jsp:include page="/includes/body/includes_after_body.jsp" />